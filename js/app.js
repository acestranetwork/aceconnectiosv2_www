var app_id='qJJlzorlprV9mAePO4kUNAbib';
var model='';
var device_token='';
var backbutton=0;
var set='';
var get_token='';
var app=angular.module('starter', ['ionic','chart.js','ngCordova','ionMDRipple','ui.rCalendar'])

app.run(function($ionicPlatform,$interval,$cordovaNetwork,$cordovaDialogs,$localstorage,$cordovaStatusbar,$state,acepush) {
 $ionicPlatform.ready(function() {
      if(!localStorage.getItem('setting') || localStorage.getItem('setting') == null ){
      /*localStorage.setItem('setting',true);*/
       localStorage.setItem('setting',JSON.stringify(set));
    }
           ionic.Platform.fullScreen();
   // $ionicPlatform.ready(function() {
   //
   // if(typeof analytics !== 'undefined') {
   //              analytics.startTrackerWithId("UA-75326736-1");
   //  } else {
   //              console.log("Google Analytics Unavailable");
   //  }
  //  if (window.cordova && window.cordova.plugins.Keyboard) {
  //   cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
  //   cordova.plugins.Keyboard.disableScroll(false);
  // }
  //
    document.addEventListener("offline", onOffline, false);
    function onOffline() {
     $cordovaDialogs.alert('Sorry, no Internet connectivity detected. Please reconnect and try again.', 'No Internet Connection', 'OK').then(function() {});
    }


 model=ionic.Platform.device();
   var push = PushNotification.init({ "ios": {"alert": "true", "badge": "true", "sound": "true","clearBadge": true}});
  push.on('registration', function(data) {
    $localstorage.set("Notification_token",data.registrationId);
    AppToken=data.registrationId;
    get_token=JSON.parse(localStorage.getItem('AppToken'));
    if(get_token != AppToken || !get_token || get_token=='' || get_token==null){
      localStorage.setItem('AppToken',JSON.stringify(AppToken));
      var obj={'app_id':app_id,'device_token':AppToken,'model':model.model,'platform':model.platform,'status':true}
        acepush.notification(obj);
        /*mylaipush.notification(obj);*/
    }
  });
 /* var push = PushNotification.init({"android":{ "senderID": "486202858374"}});
  push.on('registration', function(data) {
   $localstorage.set("Notification_token",data.registrationId);
  });*/
  push.on('notification', function(data) {
    var a=data.additionalData.openUrl.split("_");
    /*if(a[0]=="notification"){
      $state.go('menu.dashboard');
    }*/
  });
  push.on('error', function (e) {
      alert(JSON.stringify(e.message));
    // console.log(e.message);
});


})
});

app.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider,$httpProvider,$provide) {
  $ionicConfigProvider.views.maxCache(0);
  $ionicConfigProvider.scrolling.jsScrolling(false);
  $stateProvider
// setup an abstract state for the tabs directive
.state('login', {
  url: '/login',
  templateUrl: 'templates/login.html',
  controller: 'LoginCtrl'
})
.state('menu', {
  url: '/menu',
  abstract: true,
  templateUrl: 'templates/menu.html',
  controller: 'AppCtrl'
})
.state('menu.dashboard', {
  url: '/dashboard',
  views: {
    'menuContent': {
      templateUrl: 'templates/dashboard.html',
      controller: 'DashboardCtrl'
    }
  }
})
.state('menu.studentdetails', {
  url: '/studentdetails/:sid',
  params: {obj:null},
  views: {
    'menuContent': {
      templateUrl: 'templates/studentdetails.html',
      controller: 'StudentDetailsCtrl'
    }
  }
})
.state('menu.details', {
  url: '/details/:sid',
  params: {obj:null},
  views: {
    'menuContent': {
      templateUrl: 'templates/details.html',
      controller: 'DetailsCtrl'
    }
  }
})
// if none of the above states are matched, use this as the fallback
if(localStorage.getItem("token") == "" || localStorage.getItem("token") == null){
  $urlRouterProvider.otherwise('/login');
}
else{
  $urlRouterProvider.otherwise('/menu/dashboard');
}
$provide.decorator('$state', function($delegate, $stateParams) {
  $delegate.forceReload = function() {
    return $delegate.go($delegate.current, $stateParams, {
      reload: true,
      inherit: false,
      notify: true
    });
  };
  return $delegate;
});
});

app.factory('UrlService', function() {
  return {
   check: 'http://aceclassroom.com/api/',
};
});

app.factory('ClockSrv', function($interval){
  var clock = null;
  var service = {
    startClock: function(fn){
      if(clock === null){
        clock = $interval(fn, 15000);
      }
    },
    stopClock: function(){
      if(clock !== null){
        $interval.cancel(clock);
        clock = null;
      }
    }
  };
  return service;
})

function checkConnection() {
  var networkState = navigator.network.connection.type;
  var states = {};
  states[Connection.UNKNOWN]  = 'Unknown connection';
  states[Connection.ETHERNET] = 'Ethernet connection';
  states[Connection.WIFI]     = 'WiFi connection';
  states[Connection.CELL_2G]  = 'Cell 2G connection';
  states[Connection.CELL_3G]  = 'Cell 3G connection';
  states[Connection.CELL_4G]  = 'Cell 4G connection';
  states[Connection.CELL]     = 'Cell generic connection';
  states[Connection.NONE]     = 'No network connection';
  alert('Connection type: ' + states[networkState]);
}
