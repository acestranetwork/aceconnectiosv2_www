﻿app.service('SchoolInfo', function() {
// For the purpose of this example I will store user data on ionic local storage but you should save it on a database
var set = function(data) {
  window.localStorage.UserInfo = JSON.stringify(data);
};
var get = function(){
  return JSON.parse(window.localStorage.UserInfo || '{}');
};
return {
  get: get,
  set: set
};
});
app.factory('acepush', function(ajax) {
  return {
    notification: function(obj) {
      ajax.post('http://push.mylaporetoday.in/device_register', obj).then(function(result) {
        if (result.status == "success") {
        } else if (result.status == "error") {
          console.log(result.message);
        }
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          console.log("Push Api" + error);
        }
      });
    },
  };
});
app.factory('ajax', function($http,$q,UrlService,$window) {
  return {
    post: function(url,obj) {
      var deferred = $q.defer();
      $http.post(url, obj)
      .success(function(data) {            
        deferred.resolve(data);
      }).error(function(msg, code) {
        deferred.reject(code);            
      });
      return deferred.promise;
    }, 
  };
});

app.filter('myFormat', function() {
    return function(x) {
      var format = "";
      format= x.split(".").pop();      
      return format;
    };
});

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];
      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });
      return output;
   };
});

app.factory('browser', function($cordovaInAppBrowser,$rootScope) {
  return {
    open: function(obj) {
     var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'yes',
      closebuttoncaption: 'Close',
      transitionstyle:'crossdissolve'
    };

    $cordovaInAppBrowser.open(obj, '_blank', options)  
    .then(function(event) {
      console.log("success");
    }) 
    .catch(function(event) {
      console.log("failure");
    });
  }
}
}); 

app.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);
app.directive("moveNextOnMaxlength", function() {
    return {
        restrict: "A",
        link: function($scope, element) {
            element.on("input", function(e) {
                if(element.val().length == element.attr("maxlength")) {
                    var $nextElement = element.next();
                    if($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
});