﻿app.controller('LoginCtrl', function ($scope,ajax,SchoolInfo,$state,UrlService,$ionicLoading,acepush,$cordovaDialogs,$ionicHistory,$localstorage,$ionicModal, $timeout) {
  angular.element(document.querySelector(".login")).removeClass("scroll-content");
  $scope.parentId = localStorage.getItem("UserID");
  this.password = ""; 
  $scope.clearvalue = function() { 
    this.parentId = ""; 
    localStorage.setItem("UserID", "");    
    localStorage.removeItem("UserID"); 
  }
  $scope.inputType = 'password';
  $scope.view = function(){
    $scope.inputType = $scope.inputType === "text" ? "password" : "text";
  };
// Create the login modal that we will use later
$ionicModal.fromTemplateUrl('templates/forgot.html', {
  scope: $scope
}).then(function(modal) {
  $scope.modal = modal;
});
// Triggered in the login modal to close it
$scope.close = function() {
  $scope.modal.hide();
  $scope.step1=true; $scope.step2=false;   $scope.step3=false;
};
// Open the login modal
$scope.reset = function() {
  $scope.modal.show();
};
$scope.step1=true; $scope.step2=false;   $scope.step3=false;
// Perform the login action when the user submits the login form
$scope.resetpwd = function(resetid) { 
  if(!resetid){
    $cordovaDialogs.alert('Please enter parentId', 'aceConnect', 'OK').then(function() {});
  }else{
    var obj={ parentid : resetid , password :  "password" }; 
    $ionicLoading.show({template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'});
    ajax.post(UrlService.check+'LoginApi',obj).then(function(root) { 
      if(root.Url =="" || root.Url ==null){
        $ionicLoading.hide();
        $cordovaDialogs.alert('Invalid Parent Id', 'aceConnect', 'OK').then(function() {});
      }else{
        $ionicLoading.hide();
        var url=root.Url;
        SchoolInfo.set({SchoolUrl:url.SchoolUrl,AppMenu:root.AppMenu,SchoolLogo:url.SchoolLogo,SchoolWebsite:url.SchoolWebsite}); 
        var obj1={parentId :resetid};
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'});
        ajax.post(url.SchoolUrl+'api/aceConnect/Forgot',obj1).then(function(result) { 
          if(result.UserValidation =="success"){
            $ionicLoading.hide();
            $scope.step1=false; $scope.step2=true; $scope.step3=false;
            $scope.keycontent=result.message;
            localStorage.setItem("UserID",resetid); 
          }else{
            $ionicLoading.hide();
            $cordovaDialogs.alert(result.message, 'aceConnect', 'OK').then(function() {});
          }
        },function(error){
          $ionicLoading.hide();
          if(error == 500 || error==404 || error == 0){
            $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
          }
        }); 
      }
    },function(error){
      $ionicLoading.hide();
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
      } 
    });
}
}
$scope.verify=function(k1,k2,k3,k4){
  if(!k1 || ! k2 || !k3 || !k4){
    $cordovaDialogs.alert('Please enter verify code', 'aceConnect', 'OK').then(function() {});
  }else{
    var key=k1.toString()+k2.toString()+k3.toString()+k4.toString();   
    var user=localStorage.getItem("UserID"); 
    var obj2={parentId :user,code:key};
    var School=SchoolInfo.get(); 
    $ionicLoading.show({template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'});
    ajax.post(School.SchoolUrl+'api/aceConnect/Verify',obj2).then(function(result) { 
      if(result.UserValidation =="success"){
        $ionicLoading.hide();
        $scope.step1=false; $scope.step2=false; $scope.step3=true;
        $scope.keycontent=result.message;              
      }else{
        $ionicLoading.hide();
        $cordovaDialogs.alert(result.message, 'aceConnect', 'OK').then(function() {});
      }
    },function(error){
      $ionicLoading.hide();
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
      }
    });         
  }
}
$scope.clean=function(){
  this.resetId='';
  this.key1='';this.key2='';this.key3='';this.key4='';
  this.newpassword='';
  this.confirmpassword='';
}
$scope.change=function(npwd,cpwd){
  if(!npwd || !cpwd){
    $cordovaDialogs.alert('Please enter verify code', 'aceConnect', 'OK').then(function() {});
  }else if(npwd != cpwd ){
    $cordovaDialogs.alert('Incorrect confirm password', 'aceConnect', 'OK').then(function() {});
  }else{    
    var user=localStorage.getItem("UserID"); 
    var obj3={parentId :user,password:cpwd};
    var School=SchoolInfo.get(); 
    $ionicLoading.show({template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'});
    ajax.post(School.SchoolUrl+'api/aceConnect/Reset',obj3).then(function(result) { 
      if(result.UserValidation =="success"){
        $ionicLoading.hide();
        $cordovaDialogs.alert(result.message, 'aceConnect!', 'OK').then(function() {});
        $scope.close();
      }else{
        $cordovaDialogs.alert(result.message, 'aceConnect', 'OK').then(function() {});
      }
    },function(error){
      $ionicLoading.hide();
      if(error == 500 || error==404 || error == 0){
        $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
      } 
    });
  }
}

$scope.login = function(parentId,password) {  
  if(!parentId || !password){
    $cordovaDialogs.alert('Please enter all fields', 'Login failed!', 'OK').then(function() {});
  }else if(parentId.length <2){
    $cordovaDialogs.alert('Parent Id minimum 2 characters required', 'Login failed!', 'OK').then(function() {});
  }else{
    $ionicLoading.show({template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'})
    var obj={ parentid : parentId , password :  password }
    ajax.post(UrlService.check+'LoginApi',obj).then(function(root) { 
      if(root.Url =="" || root.Url ==null){
        $ionicLoading.hide();
        $cordovaDialogs.alert('Invalid Parent Id', 'Login failed!', 'OK').then(function() {});
      }
      else
      {
        var url=root.Url;
        SchoolInfo.set({SchoolUrl:url.SchoolUrl,AppMenu:root.AppMenu,SchoolLogo:url.SchoolLogo,SchoolWebsite:url.SchoolWebsite}); 
        var School=SchoolInfo.get();  
        var AppToken=$localstorage.get("Notification_token");
        var obj={ parentid : parentId , password :  password , ntoken : AppToken , platform : "ios" }; 
    var obj1={'app_id':app_id,'device_token':AppToken,'model':model.model,'platform':model.platform,'status':true}  
        ajax.post(School.SchoolUrl+'api/aceConnect/Login',obj).then(function(result) { 
          if(result == "Incorrect password"){
            acepush.notification(obj1);
            $ionicLoading.hide();
            $cordovaDialogs.alert('Incorrect password', 'Login failed!', 'OK').then(function() {});
          }else if(result == "Incorrect username and password"){
            $ionicLoading.hide();
            $cordovaDialogs.alert('Incorrect username and password', 'Login failed!', 'OK').then(function() {});
          }else{
            localStorage.setItem("UserID",parentId);
            $ionicHistory.nextViewOptions({
              disableAnimate: true,
              disableBack: true
            });  
            /*var AppToken=$localstorage.get("Notification_token");
            var obj={token:result,ntoken:AppToken,platform:"ios"};
            ajax.post(School.SchoolUrl+'api/aceConnect/PushNotification',obj).then(function(result) { 
            })*/
            localStorage.setItem("token", result); 
            localStorage.setItem("UserID", parentId);        
            $state.go('menu.dashboard',{}); 
            $scope.password = ""; 
          }                      
        },function(error){
          $ionicLoading.hide();
          if(error == 500 || error==404 || error == 0){
            $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
          }   
        }); 
}    
},function(error){
  $ionicLoading.hide();
  if(error == 500 || error==404 || error == 0){
    $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
  }
}); 
}
} 
});