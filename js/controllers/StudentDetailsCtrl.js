app.controller('StudentDetailsCtrl', function ($scope,$filter,$ionicLoading,$ionicModal,$window,$cordovaDialogs,$rootScope,$stateParams,SchoolInfo,$ionicActionSheet,$window,$http,UrlService,ajax,$ionicHistory,$ionicSlideBoxDelegate,$cordovaDatePicker,$ionicScrollDelegate) {
function getTimeZone() {
    var offset = new Date().getTimezoneOffset(),
        o = Math.abs(offset);
    return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
}
var tz=getTimeZone();
$scope.id = localStorage.getItem("token");
  angular.element(document.querySelector(".remove")).removeClass("scroll-content");
  var sobject=$stateParams.obj;
  $scope.sid=sobject.StudentRegId;
  var School=SchoolInfo.get();
  $scope.image=School.SchoolUrl+'Documents/'+sobject.StudentId+'/'+sobject.Photo;
  $scope.firstname=sobject.StudentFirstName;
  $scope.lastname=sobject.StudentLastName;
  $scope.uploadimg=School.SchoolUrl+'Upload/Image';
  $scope.hwuploadfile=School.SchoolUrl+'Documents/Homework';
  $scope.uploadfile=School.SchoolUrl+'Areas/Communication/Documents';
  var menu = School.AppMenu;
    $scope.AppMenu = School.AppMenu;

  $rootScope.activeTabMenus=true;
  $rootScope.activeMenu = $scope.AppMenu[0].MenuKey;

for(i=0;i<menu.length;i++){
  if(menu[i].MenuKey=="album"){
   /* $scope.menu1=true;
    $scope.menuname1=menu[i].MenuName;*/
///////////////////////////////////album////////////////////////////////////////
$scope.sdate = new Date();
var flag=1;
$scope.albums = function(sdata) {
  $scope.date = $filter('date')(sdata, 'yyyy/MM/dd',tz);//December-November like
  var obj={ token : $scope.id , studentRegId : $scope.sid, date : $scope.date };
  ajax.post(School.SchoolUrl+'api/aceConnect/Album',obj).then(function(result) {
    if( result.UserValidation =="Success")
    {
      $scope.album = result.List;
      $scope.otherimages=$scope.album.AlbumImg;
      console.log(JSON.stringify(result.List));
      if($scope.album.length==0){
        var c_temp = $scope.date.split('/');
        var c_year=c_temp[0];
        var c_months=Number(c_temp[1])-1;
        if(c_months==0){
          var c_year=Number(c_year)-1;
          var c_months=12;
        }
        $scope.date = c_year+"/"+c_months+"/"+1 ;
        if(flag < 3){
          // albums();
          flag=flag+1;
        }
      }
    }
    else
    {
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
    }
  });
}
// albums();
$scope.load=function()
{
  var temp = $scope.date.split('/');
  var year=temp[0];
  var months=Number(temp[1])-1;
  if(months==0){
    var year=Number(year)-1;
    var months=12;
  }
  $scope.date = year+"/"+months+"/"+1 ;
  var obj={ token : $scope.id , studentRegId : $scope.sid, date : $scope.date};
  ajax.post(School.SchoolUrl+'api/aceConnect/Album',obj).then(function(result) {
    if( result.UserValidation =="Success")
    {
      angular.forEach(result.List,function(item){
        $scope.album.push(item);
      })
    }else{
      $cordovaDialogs.alert("You've reached the end of our list.", 'aceConnect', 'OK').then(function() {});
    }
  });
};
}else if(menu[i].MenuKey=="attendance"){
 /* $scope.menu2=true;
  $scope.menuname2=menu[i].MenuName;*/
///////////////////////////////////Attendance////////////////////////////////////////
var obj={ token : $scope.id , studentRegId : $scope.sid };
ajax.post(School.SchoolUrl+'api/aceConnect/Attendance',obj).then(function(result) {
  if( result.UserValidation =="Success" ){
    $scope.labels = result.getAttendanceMonth.monthName;
    $scope.series = ['Working Days','Present Days'];
    $scope.data = [result.getAttendanceMonth.NumberOfWorkingDays,result.getAttendanceMonth.presentDaysCount];
  }
});
}else if(menu[i].MenuKey=="homework"){
/*  $scope.menu3=true;
  $scope.menuname3=menu[i].MenuName;*/
///////////////////////////////////Homework////////////////////////////////////////
$scope.selectcss = function (s) {
  var file= s.split(".").pop();
  if (file == 'pdf')
    return 'pdf';
  else
    return 'docx';
};
$scope.opendoc = function (s) {
  window.open(s,'_system');
};
$scope.hwdate =new Date();
$scope.gethomework = function(hw) {
  var obj={ token : $scope.id , studentRegId : $scope.sid, hwdate : hw};
  ajax.post(School.SchoolUrl+'api/aceConnect/Homework',obj).then(function(result) {
    if( result.UserValidation =="Success")
    {
      $scope.homework = result.List;
    }
    else
    {
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
    }
  });
};
var obj={ token : $scope.id , studentRegId : $scope.sid };
ajax.post(School.SchoolUrl+'api/aceConnect/Homework',obj).then(function(result) {
  if( result.UserValidation =="Success")
  {
    $scope.homework = result.List;
  }
  else
  {
    $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
  }
});
}else if(menu[i].MenuKey=="communication"){
/*  $scope.menu4=true;
  $scope.menuname4=menu[i].MenuName;*/
///////////////////////////////////Communication////////////////////////////////////////

$scope.selectcss = function (s) {
  var file= s.split(".").pop();
  if (file == 'pdf')
    return 'pdf';
  else
    return 'docx';
};
$scope.opendoc = function (s) {
  window.open(s,'_system');
};

$scope.cdate =new Date();

$scope.getcommunication = function(c) {
  var obj={ token : $scope.id , studentRegId : $scope.sid, date : c };
  ajax.post(School.SchoolUrl+'api/aceConnect/Communication',obj).then(function(result) {
    if( result.UserValidation =="Success"){
      $scope.communication = result.List;
    } else {
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
    }
  });
};
var obj={ token : $scope.id , studentRegId : $scope.sid };
ajax.post(School.SchoolUrl+'api/aceConnect/Communication',obj).then(function(result) {
  if( result.UserValidation =="Success"){
    $scope.communication = result.List;
  } else {
    $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
  }
});
}else if(menu[i].MenuKey=="calendar"){
/*  $scope.menu5=true;
  $scope.menuname5=menu[i].MenuName;*/
///////////////////////////////////Calendar////////////////////////////////////////
$scope.changeMode = function (mode) {
  $scope.mode = mode;
};
$scope.today = function () {
  $scope.currentDate = new Date();
};
$scope.isToday = function () {
  var today = new Date(),
  currentCalendarDate = new Date($scope.currentDate);
  today.setHours(0, 0, 0, 0);
  currentCalendarDate.setHours(0, 0, 0, 0);
  return today.getTime() === currentCalendarDate.getTime();
};
$scope.loadEvents = function () {
  createRandomEvents();
  $rootScope.$on("events", function(event, LeaveRequestlistcount) {
    $scope.eventSource = LeaveRequestlistcount;
  });
};
$scope.onEventSelected = function (event) {
  $scope.event = event;
};
$scope.onTimeSelected = function (selectedTime) {
};

function createRandomEvents() {
  var events = [];
  var obj={ token : $scope.id , studentRegId : $scope.sid };
  ajax.post(School.SchoolUrl+'api/aceConnect/Calendar',obj).then(function(result) {
    if( result.UserValidation =="Success")
    {
      var cal = result.List;
      angular.forEach(cal, function(value, key){
        var dated=new Date(value.EventDate);
        var dates =new Date(Date.UTC(dated.getUTCFullYear(), dated.getUTCMonth(), dated.getUTCDate()));
        events.push({title: value.Title,desc:value.Description,startTime:dates,endTime:dates,allDay:false});
      });
      $rootScope.$broadcast("events",events);
    }
  });
  return events;
}
}else if(menu[i].MenuKey=="leaverequest"){
/*  $scope.menu6=true;
  $scope.menuname6=menu[i].MenuName;*/
///////////////////////////////////Leave Reaquest////////////////////////////////////////

$http.get(School.SchoolUrl+'api/aceConnect/LeaveRequest/'+$scope.sid).then(
  function(response){
    $scope.leaverequestlist=response.data;
  }, function(error){
    console.log("failure connect to api leave request");
  }
  );
$scope.leaveview = function(view) {
  $cordovaDialogs.alert('Reason : '+view.reason, 'Date :'+view.fdate+' - '+view.tdate, 'OK').then(function() {});
}
$scope.fromdate = new Date();
$scope.todate = new Date();
$scope.leaveapply = function(reason,fromdate,todate) {
  if(!reason || !fromdate || !todate){
    $cordovaDialogs.alert('Please enter all fields','Error', 'OK').then(function() {});
    $cordovaDialogs.beep(1);
  }
  else{
    $ionicLoading.show({template: ' <ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'})
    $scope.result = {};
    var obj={ token : $scope.id , studentRegId : $scope.sid, fromdate : fromdate , todate : todate , reason : reason };
    ajax.post(School.SchoolUrl+'api/aceConnect/LeaveRequest',obj).then(function(result) {
      $ionicLoading.hide();
      if(result.message== "Successfully applied your leave request"){
        $cordovaDialogs.alert('Successfully Send', 'Leave Request', 'OK').then(function() {});
        $http.get(School.SchoolUrl+'api/aceConnect/LeaveRequest/'+$scope.sid).then(
          function(response){
            $scope.leaverequestlist=response.data;
          },function(response){
            console.log("failure connect to api leave request");
          }
          );
        $scope.reason="";
        $scope.fromdate = new Date();
        $scope.todate = new Date();
      }
      else{
        $cordovaDialogs.alert('Not Send', 'Leave Request', 'OK').then(function() {});
      }
    })
  }
}
$scope.clearfields = function(){
  this.reason="";
  $scope.fromdate = new Date();
  $scope.todate = new Date();
}
$scope.status=false;
}else if(menu[i].MenuKey=="fee"){
/*  $scope.menu7=true;
  $scope.menuname7=menu[i].MenuName;*/
///////////////////////////////////Fee////////////////////////////////////////

var obj={ token : $scope.id , studentRegId : $scope.sid };
ajax.post(School.SchoolUrl+'api/aceConnect/Fee',obj).then(function(result) {
  if( result.UserValidation =="Success" )
  {
    $scope.fee = result.List;
  }
  else
  {
    $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
  }
});

$scope.termfee=false;
$scope.annualfee=true;

$scope.term = function(){
  $scope.termfee=true;
  $scope.annualfee=false;
}
$scope.annual = function(){
  $scope.termfee=false;
  $scope.annualfee=true;
}

}else if(menu[i].MenuKey=="article"){
/*  $scope.menu8=true;
  $scope.menuname8=menu[i].MenuName;*/
///////////////////////////////////article////////////////////////////////////////

$scope.articledate=new Date();
$scope.getarticle = function(article) {
  var obj={ token : $scope.id , studentRegId : $scope.sid, date : article};
  ajax.post(School.SchoolUrl+'api/aceConnect/ArticleOfTheWeek',obj).then(function(result) {
    if( result.UserValidation =="Success")
    {
      $scope.article = result.List;
    }
    else
    {
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
    }
  });
};

var obj={ token : $scope.id , studentRegId : $scope.sid };
ajax.post(School.SchoolUrl+'api/aceConnect/ArticleOfTheWeek',obj).then(function(result) {
  if( result.UserValidation =="Success")
  {
    $scope.article = result.List;
  }
  else
  {
    $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'aceConnect', 'OK').then(function() {});
  }
});
}
}
$scope.myActiveSlide = 0;
if($window.innerWidth <= 320){
  $scope.height = $window.innerHeight-188+'px';
  $scope.height1 = $window.innerHeight-223+'px';
}else{
  $scope.height = $window.innerHeight-228+'px';
  $scope.height1 = $window.innerHeight-273+'px';
}

$scope.imagewidth=function(size){
  if(size ==1)
    return "width:100%"
  else if(size ==2)
    return "width:49%"
  else if(size ==3)
    return "width:32.5%"
  else if(size >=4)
    return "width:24%"
}
$ionicModal.fromTemplateUrl('image-modal.html', {
  scope: $scope,
  animation: 'slide-in-down'
}).then(function(modal) {
  $scope.modal = modal;
});

$scope.openImageModal = function(images,slide) {
  $scope.Imagepop=images;
  $scope.myActiveSlide1 = slide;
  $scope.modal.show();
};

$scope.closeModal = function() {
  $scope.modal.hide();
};

// Cleanup the modal when we're done with it!
$scope.$on('$destroy', function() {
  $scope.modal.remove();
});
// Execute action on hide modal
$scope.$on('modal.hide', function() {
// Execute action
});
// Execute action on remove modal
$scope.$on('modal.removed', function() {
// Execute action
});
$scope.$on('modal.shown', function() {
});

// Call this functions if you need to manually control the slides
$scope.next = function() {
  $ionicSlideBoxDelegate.next();
};

$scope.previous = function() {
  $ionicSlideBoxDelegate.previous();
};

$scope.goToSlide = function(index) {
  $scope.modal.show();
  $ionicSlideBoxDelegate.slide(index);
}

// Called each time the slide changes
$scope.slideChanged = function(index) {
  $scope.slideIndex = index;
};
$scope.zoomMin = 1;
  function getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;

    while (element) {
      xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
      yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
      element = element.offsetParent;
    }
    return {
      x: xPosition,
      y: yPosition
    };
  }
  $scope.getTouchposition = function(event) {
    var canvasPosition = getPosition(event.gesture.touches[0].target);
    var tap = {
      x: 0,
      y: 0
    };
    if (event.gesture.touches.length > 0) {
      tt = event.gesture.touches[0];
      tap.x = tt.clientX || tt.pageX || tt.screenX || 0;
      tap.y = tt.clientY || tt.pageY || tt.screenY || 0;
    }
    tap.x = tap.x - canvasPosition.x;
    tap.y = tap.y - canvasPosition.y;

    return {
      x: tap.x,
      y: tap.y
    };
  }
  var zoomed = true;
  $scope.onDoubleTap = function(a) {
    if (zoomed) { // toggle zoom in
      var tap = {
        x: 0,
        y: 0
      };
      var position = $scope.getTouchposition(event);
      $ionicScrollDelegate.$getByHandle('scrollHandle' + a).zoomBy(2.0, true, position.x, position.y);
      zoomed = !zoomed;
    } else { // toggle zoom out
      $ionicScrollDelegate.$getByHandle('scrollHandle' + a).zoomTo(1, true);
      zoomed = !zoomed;
    }
  }
});
