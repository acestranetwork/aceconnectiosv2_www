﻿app.controller('DashboardCtrl', function ($scope,$ionicLoading,SchoolInfo,$state,ajax,$cordovaDialogs,browser,UrlService) {
  var School=SchoolInfo.get();
  $scope.website=School.SchoolUrl;
var str = UrlService.check;
$scope.url = str.substring(0, str.length - 4);
  $scope.image=School.SchoolUrl+'Documents';
  $scope.id = localStorage.getItem("token");
  $scope.placeholder="img/placeholder.png"
  $scope.result = {};
  if(!School.SchoolLogo){
  $scope.logo="aceconnectthumb.png"
}else{
  $scope.logo=School.SchoolLogo;
}
  $ionicLoading.show({template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'})
  var obj={ token : $scope.id };
  ajax.post(School.SchoolUrl+'api/aceConnect/Dashboard',obj).then(function(result) {
    if( result.UserValidation =="Success" )
    {
      $ionicLoading.hide();
      $scope.AnnouncementList = result.AnnouncementList;
      $scope.StudentList = result.StudentList;
      $scope.PrimaryUser =result.StudentList[0].PrimaryUserFirstName;
    }else if( result.Message =="Invalid User." )
   {
   $ionicHistory.clearCache("");
   $ionicHistory.clearHistory();
   $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
   $state.go('login');
   localStorage.setItem("UserInfo", "");
   localStorage.removeItem("UserInfo");
   localStorage.setItem("token", "");
   localStorage.removeItem("token");
   $cordovaDialogs.alert('Session Timeout', 'aceConnect', 'OK').then(function() {});
   }
  },function(code){
    $ionicLoading.hide();
    if(error == 500 || code == 404 || code == 0 ){
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'AceConnect', 'OK').then(function() {});
    }
  });
  $scope.studentdetails = function(sid){
    $state.go('menu.studentdetails',{obj:sid});
  };
   $scope.openBrowser = function(link) {
    browser.open(link);
  }
 $scope.colors=[];  for(var i=0; i<10;i++){    $scope.colors[i] = ('#'+ Math.floor(Math.random()*16777215).toString(16));  }
});
