﻿app.controller('AppCtrl', function ($scope,$http,$rootScope,browser,$cordovaDialogs,SchoolInfo,ClockSrv,$stateParams,$ionicLoading,$ionicModal,$ionicHistory,$location, $ionicPopover,$timeout,ajax,$location,$state,$ionicSlideBoxDelegate,UrlService,$ionicScrollDelegate) {
  var School=SchoolInfo.get();
  var next=true;
  var str = UrlService.check;
  $scope.url = str.substring(0, str.length - 4);
  $scope.privacy='http://aceclassroom.com/Home/privacy';
  if(!School.SchoolLogo){
    $scope.logo="aceconnectthumb.png"
  }else{
    $scope.logo=School.SchoolLogo;
  }
  $scope.website=School.SchoolWebsite;
   var menu = School.AppMenu;

 $scope.AppMenu = School.AppMenu;

  $rootScope.activeTabMenus=true;
  $rootScope.activeMenu = $scope.AppMenu[0].MenuKey;
   $scope.setActive = function(menuItem,pass,menulength) {
    $rootScope.activeMenu = menuItem.MenuKey;
    if(pass<(menulength/2)){
       $rootScope.activeTabMenus=true;
     }else{
 $rootScope.activeTabMenus=false;
     }

 }
 $scope.changetab =function(){
$rootScope.activeTabMenus=!$rootScope.activeTabMenus;
 }

  $ionicModal.fromTemplateUrl('templates/about.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.close= function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.guide = function() {
    $scope.popover1.hide();
    $scope.modal.show();

  };
  $scope.image=School.SchoolUrl;

 $scope.refresh=function(){
      var obj={ token : localStorage.getItem("token") };
  ajax.post(School.SchoolUrl+'api/aceConnect/Notification',obj).then(function(result) {
    if( result.UserValidation =="Success" )
    {
    $scope.NCount=result.UnreadNotificationCount;
    if(result.UnreadNotificationList.length > 0){
      $scope.NList=result.UnreadNotificationList;
    }
    }
  });
  }
 $scope.refresh();
$scope.count= function() {
    return $scope.NCount;
  };
  ClockSrv.startClock(function(){
    if(localStorage.getItem("token") != null){
      var obj={ token : localStorage.getItem("token") };
      ajax.post(School.SchoolUrl+'api/aceConnect/Notification',obj).then(function(result) {
       if( result.UserValidation =="Success" )
       {
       $scope.NCount=result.UnreadNotificationCount;
        if(result.UnreadNotificationList.length > 0){
         $scope.NList=result.UnreadNotificationList;
       }
       }
      });
    }
  });
  var navIcons = document.getElementsByClassName('ion-navicon');
  for (var i = 0; i < navIcons.length; i++) {
    navIcons.addEventListener('click', function () {
      this.classList.toggle('active');
    });
  }
var template = '<ion-popover-view >' +
  '<ion-header-bar><h1 class="title">Notification</h1></ion-header-bar>' +
  '<ion-content delegate-handle="small">' +
  '<div ng-repeat="l in NList" class="item item-body" >'+/*|orderBy:$index:true*/
  '<a class="" ng-click="NotificationRead({{l}});" ng-class="class_{{$index}}" >'+
  '<div class="mydiv"><img ng-src="{{image}}/{{l.StudentId}}/{{l.StudentPhoto}}" class="stud-img1" ng-if="l.StudentPhoto" alt="l.StudentPhoto">'+
  '<div class="ico {{l.NotificationFrom | limitTo:2}}" ng-if="!l.StudentPhoto"></div>'+
  '<div class="sub" ng-if="l.Title"><strong><span ng-bind="l.Title "></span></strong> - <span ng-bind="l.Description"></span></div>'+
  '<div class="sub" ng-if="l.FromDate"><span ng-bind="l.FromDate"></span>-<span ng-bind="l.ToDate"></span>   </div>'+
  '<div class="sub" ng-if="l.LeaveRequestStatus"><strong>Staus :</strong> <span ng-bind="l.LeaveRequestStatus"></span>  </div>'+
  '<div class="sub dates" ng-if="l.PostedDate"><span class="from" >{{l.NotificationFrom | limitTo:1}}</span><i class="icon ion-calendar"></i><span ng-bind="l.PostedDate"></span>  </div>'+
  '</div></a>'+
  '</div>' +
  '<p ng-show="NList == 0 || !NList" class="center clear" ><span class="">No updates</span></p>'+
/*  '<p ng-show="NList" class="center clear {{toggle(NList)}}" ng-click="clearall()" ><span class="">Clear All</span></p>'+*/
  '</ion-content>' +
  '</ion-popover-view>';

  var template1 = '<ion-popover-view class="smallpop">' +
  '<ion-content delegate-handle="small">' +
  '<div class="list">'+
  '<a class="item item-icon-left" ng-click="guide()" href="#"><i class="icon ion-ios-help-outline"></i>  User Guide </a>'+
  '<a class="item item-icon-left" ng-click="openprivacy()" href="#"><i class="icon ion-ios-unlocked-outline"></i>  Privacy Policy </a>'+
  '<p class="item item-icon-left"><i class="icon ion-ios-bell-outline"></i><ion-toggle toggle-class="toggle-balanced" ng-model="push" ng-checked="true" ng-click="setting()">Notification Alert ?</ion-toggle></p>'+
  '<a class="item item-icon-left" ng-click="logout()" href="#"><i class="icon"><img ng-src="img/logout.png" style="margin-left:9px;"/></i> Logout </a>'+ '</div>'+
  '</ion-content>' +
  '</ion-popover-view>';
  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
   $scope.popover1 = $ionicPopover.fromTemplate(template1, {
    scope: $scope
  });
$scope.setting = function() {
    $scope.push = !$scope.push;
    localStorage.setItem('setting', $scope.push);
    var notiobj = {
      'token' : localStorage.getItem("token"),
      'status': $scope.push
    }
      ajax.post(School.SchoolUrl+'api/aceConnect/PushStatus',notiobj).then(function(result) {
     $cordovaDialogs.alert(result.UserValidation, 'Notification', 'OK').then(function() {});

   });
  }
  $scope.clearall=function(){
    $scope.NList=[];
    $ionicScrollDelegate.$getByHandle('small').scrollTop();
  }
  $scope.closePopover = function () {
    $scope.popover.hide();
    $scope.popover1.hide();
  };
  $scope.readcolor = function (id) {
    var clas = "class_"+id;
    $scope[clas] = "readed";
  }
  $scope.toggle = function (data) {
    if (data == 0)
      return 'hidden';
    else
      return 'show'
  };
   $scope.NotificationRead =function(data){
  if(data.NotificationFrom=='ALBUM'){
    $http.get(School.SchoolUrl+'api/aceConnect/Album/'+data.AlbumId).then(function(result) {
      $rootScope.$broadcast("AlbumImage",result.data);
      refresh();
    });
    $scope.NList=[];
  }

    data.token=localStorage.getItem("token");
   ajax.post(School.SchoolUrl+'api/aceConnect/NotificationRead',data).then(function(result) {
    });
   $scope.NList=[];
    $state.go('menu.details',{obj:data});
    $scope.closePopover();
  };

  $scope.openprivacy = function(link) {
    $scope.closePopover();
    browser.open($scope.privacy);
  }

 /* $scope.clear=function(){
    $scope.NCount= 0 ;
    if($scope.NList > 100){
     $scope.NList=[];
    }
    $scope.token = localStorage.getItem("token");
    var obj={ token : $scope.token };
    ajax.post(School.SchoolUrl+'api/aceConnect/NotificationRead',obj).then(function(result) {
    });
} */
$scope.logout = function() {
  $scope.popover1.hide();
  $ionicLoading.show({template: ' <ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'})
  $scope.result={};
  var obj={ token : localStorage.getItem("token") };
  ajax.post(School.SchoolUrl+'api/aceConnect/Logout',obj).then(function(result) {
    $ionicLoading.hide();
    ClockSrv.stopClock();
    if( result =="Logout successfully" )
    {
      $ionicHistory.clearCache("");
      $ionicHistory.clearHistory();
      $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
      $state.go('login');
      localStorage.setItem("UserInfo", "");
      localStorage.removeItem("UserInfo");
      localStorage.setItem("token", "");
      localStorage.removeItem("token");
    }
    else{
     $cordovaDialogs.alert(result, 'Communication Error!', 'OK').then(function() {});
    }
  });
};

});
