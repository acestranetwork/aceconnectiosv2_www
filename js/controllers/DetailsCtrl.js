﻿app.controller('DetailsCtrl', function ($scope,$stateParams,SchoolInfo,$rootScope,$ionicModal) {
  var School=SchoolInfo.get();
  $scope.data=$stateParams.obj;
  $scope.image=School.SchoolUrl+'Documents/'+$scope.data.StudentId+'/'+$scope.data.StudentPhoto;
  $scope.studentname=$scope.data.StudentName;
  $scope.uploadimg=School.SchoolUrl+'Upload/Image';
  $scope.hwuploadfile=School.SchoolUrl+'Documents/Homework';
  $scope.uploadfile=School.SchoolUrl+'Areas/Communication/DocumentS';
  $rootScope.$on("AlbumImage", function(event, LeaveRequestlistcount) {
    $scope.AlbumImage = LeaveRequestlistcount;
  });
  $scope.file = function (data) {
    var file= data.split(".").pop();
    if (file == 'pdf')
      return 'pdf';
    else
      return 'docx';
  };
    $scope.opendoc = function (data) {
  window.open(data,'_system');
};
  $scope.imagewidth=function(size){
  if(size ==1)
    return "width:100%"
  else if(size ==2)
    return "width:49%"
  else if(size ==3)
    return "width:32.5%"
  else if(size >=4)
    return "width:24%"
   }
    $ionicModal.fromTemplateUrl('image-modal.html', {
      scope: $scope,
      animation: 'fade-in'
    }).then(function(modal) {
      $scope.modal = modal;
    });
    $scope.openImageModal = function(images,slide) {
      $scope.Imagepop=images;
      $scope.myActiveSlide1 = slide;
      // $ionicSlideBoxDelegate.slide(slide);
      $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
      // Execute action
    });
    $scope.$on('modal.shown', function() {
      console.log('Modal is shown!');
    });

    // Call this functions if you need to manually control the slides
    $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };
    $scope.goToSlide = function(index) {
      $scope.modal.show();
      $ionicSlideBoxDelegate.slide(index);
    }
    // Called each time the slide changes
    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
    };
});
